![](https://i.imgur.com/CjoDjCT.png )


## Introduction



Les équipes de Ripple Motion utilisent des Post-Its afin de matérialiser les tâches à effectuer dans un projet.  Organisées autour de la méthode Agile.

* Chaque projet est un backlog.
* Dans le backlog ya des elements backlogs.
* Les elments de backlog sont decoupé en taches.

Les développeurs énumèrent les user-stories et tâches à effectuer dans un backlog (type CSV), ils alimentes ce backlog au fur et a mesur, le backlog est ensuite transformée en PostIts.

Cette dernière étape est très consommatrice de temps car réalisée à la main.

L’objectif est d’automatiser l’exportation du backlog vers un BackLog imprimable en format PDF contenant tous les Post-It. 


## Spec “outil en ligne de commande“ (étape 1)

De manière générale, un post it représente une tâche avec un identifiant unique par projet, un titre, une description, un ensemble de tags, une user story ainsi qu’un estimation de la charge. A noter que la description et/ou les tags peuvent être absents. 


L’outil de génération doit fonctionner en ligne de commande , ex: ./manage.py import myBacklog.csv) 

* Les paramètres de ligne de commande seront: •Chemin du fichier CSV •Nom du projet (optionnel)  
* Le format du fichier CSV en entrée est décrit en annexe. Chaque ligne décrit une tâche. Chaque tâche est associée à une User Story. 
* L’outil regroupera les tâches par User Story 
* Il assignera un identifiant unique à chaque User Story 
* L’outil créera un Post-it pour chaque User Story et chaque tâches suivant le template en annexe (PDF avec 6 Post-It par page) 
* Le fichier PDF généré aura pour {project_name}_{timestamp}.pdf. Ce dernier pourra être généré à l’aide d’une action de l’admin sur le modèle “Backlog”


## Spec “Serveur / App Mobile” (étape 2*)

Une application mobile affichera une représentation du backlog sous la forme de  trois écrans (maquette à réaliser et à valider avec le client):  
- Une liste des backlogs 
- Une liste de UserStory d’un backlog 
- Une liste de Tâches d’une UserStory 
- Un écran de détail d’un tache éditable*  

Le serveur exposera les APIs suivantes: 

- De liste des backlogs 
- De détail de backlog décrivant les User Story et Tâches associées 
- *D’ajout d’une UserStory pour un backlog 
- *De suppression d’une UserStory pour un backlog 
- *D’ajout d’une Tâche pour une User Story 
- *De modification d’une Tâche pour une User Story 
- *De suppression d’une Tâche pour une User Story

> *(bonus)


## Schema base données :

![](https://i.imgur.com/U5zasGf.png)

## Documentation Technique :


[Clique ici ](https://forge.iut-larochelle.fr/mtajaoua/post-it_generator/-/wikis/Documentation-technique), ou allez dans la partie Wiki du projet.
