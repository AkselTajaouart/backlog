#!/bin/bash

set -e

cd "$(dirname "$0")"

IMAGE=quay.io/ripplemotion/postit-generator
TAG=`git rev-parse --abbrev-ref HEAD`

tox -e package
docker build -t $IMAGE:$TAG .
docker push $IMAGE:$TAG
