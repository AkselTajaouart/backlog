import os
import unittest

from main import Main


class TestCSVConverter(unittest.TestCase):
    def test_convert_csv(self):
        """
        Given  a well formated csv
        When I try convert this csv to a pdf
        Then I get a pdf file
        """
        dirname = os.path.dirname(__file__)
        csv_path = os.path.join(dirname, 'csv', 'correct.csv')
        pdf_path = Main.run(f'{csv_path}')
        self.assertTrue(os.path.exists(pdf_path))

    def test_convert_csv__incorrect_csv(self):
        """
        Given  a csv with syntax error
        When I try convert this csv to a pdf
        Then I get an error
        """
        dirname = os.path.dirname(__file__)
        csv_path = os.path.join(dirname, 'csv', 'incorrect.csv')
        pdf_path = Main.run(f'{csv_path}')
        self.assertFalse(os.path.exists(str(pdf_path)))


if __name__ == '__main__':
    unittest.main()
