import os
import shutil
from datetime import date

from django.db import models
from django.test import Client
from PyPDF2 import PdfFileMerger
from weasyprint import HTML

from postit_generator.api.v10.tests.fixtures import PostitAPIMixin


class BackLog(models.Model):
    project_name: str = models.CharField(max_length=50)
    last_edit_date = models.DateField()
    done: bool = models.BooleanField(default=False, blank=True)

    def __str__(self) -> str:
        return f'{self.project_name} (done={self.done})'

    def complete(self) -> 'BackLog':
        self.done = True
        self.save()
        return self


class CSV(models.Model):
    project_name: str = models.CharField(max_length=50)
    txt_backlog: str = models.TextField()

    def __str__(self) -> str:
        return self.project_name


class US(models.Model):
    id_us: str = models.CharField(max_length=10)  # id given by user
    back_log: BackLog = models.ForeignKey(BackLog, on_delete=models.CASCADE)
    title: str = models.CharField(max_length=250)
    last_edit_date = models.DateField()

    def __str__(self) -> str:
        return self.title


class Task(models.Model):
    us: US = models.ForeignKey(US, on_delete=models.CASCADE)
    last_edit_date = models.DateField()
    id_task: str = models.CharField(max_length=10)  # id of task :  (US + (number))
    tags: str = models.CharField(max_length=250)  # each tag separated by ,
    title: str = models.CharField(max_length=100)
    description: str = models.CharField(max_length=250)
    load: int = models.IntegerField()

    def __str__(self) -> str:
        return self.title


class PostItGenerator(PostitAPIMixin):
    main_path = os.path.dirname(__file__)
    if main_path == "":
        main_path = "postit_generator/"
    else:
        main_path = main_path + "/postit_generator/"
    # Array of postits which represents each row in the CSV file
    postits = []  # type: ignore
    actual_US_index_in_postits = -1
    index_task = 1
    # list of US
    us_list = []   # type: ignore
    task_list = []   # type: ignore
    client = Client()
    # the ID of US is represented by a charachter,
    #  after Z this ID will represented with nbrChar + 1, ex : AA
    doubeChar = 1
    # the char the represents the US's Id is stored
    # in ascci code to facilate incrementation
    ascci_indx_us = 64
    # number of postits
    nbr_postit = 0
    # TODO rename cpt
    cpt = 1
    # variable that will store html code of html pages
    html = ""
    # number of html generated pages
    document_nbr = 1
    # the html code of page template will be stored as string
    data = ""
    # the project name
    project_name = ""

    _tag = "$TAG_"
    _id = "$IDENTIFIER_"
    _title = "$TITLE_"
    _desc = "$DESCRIPTION_"
    _point = "$POINT_"

    # replace tags with postit information
    def replace_tags(self, postit, html):
        id = "#"+str(postit.id)
        html = html.replace(self._id+str(self.cpt), id)
        html = html.replace(self._tag+str(self.cpt), "["+postit.tag+"]")
        html = html.replace(self._title+str(self.cpt), postit.title)
        desc = postit.description.replace("\n", ".<br><br>")
        desc = "<br>" + desc
        html = html.replace(self._desc+str(self.cpt), desc)
        html = html.replace(self._point+str(self.cpt), str(postit.load))
        return html

    class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'

    # representation of the postit data
    class Postit:
        def __init__(self, group, tag, id, title, description, load):
            self.group = group
            self.tag = tag
            self.id = id
            self.title = title
            self.description = description
            self.load = load

    # prepare folders where to save generated files
    def setup_folders(self):
        if os.path.exists(self.main_path+"PDF_Output"):
            shutil.rmtree(self.main_path+"PDF_Output")
        os.mkdir(self.main_path+"PDF_Output")
        if os.path.exists(self.main_path+"HTML_Output"):
            shutil.rmtree(self.main_path+"HTML_Output")
        os.mkdir(self.main_path+"HTML_Output")
        css_path = self.main_path+"template/page.css"
        shutil.copy(css_path, self.main_path+"HTML_Output/page.css")

    # get char from ascci code
    def get_char_id(self, ascci):
        id = ""
        for x in range(self.doubeChar):
            id = id + chr(ascci)
        return id

    # read CSV file and save each row data as postit into array
    def read_csv(self, csv_txt):
        # global us_list, ascci_indx_us, doubeChar
        # global index_task, postits, nbr_postit
        nbr_rows = 0
        csv_txt = csv_txt.split('\n')
        for row in csv_txt:
            row = str(row)
            row = row.split(';')
            nbr_rows = nbr_rows + 1
            # TODO: extract if block in a function
            if row[0] not in self.us_list:
                if self.ascci_indx_us >= 90:
                    self.doubeChar = 2
                    self.ascci_indx_us = 64
                self.index_task = 1
                self.ascci_indx_us = self.ascci_indx_us + 1
                char = self.get_char_id(self.ascci_indx_us)
                us = self.Postit("", "", char, "US : "+row[0], "", 0)
                self.postits.append(us)
                self.nbr_postit = self.nbr_postit + 1
                self.actual_US_index_in_postits = self.nbr_postit
                self.us_list.append(row[0])
            group = row[0]
            tag = row[1]
            title = row[2]
            description = row[3]
            load = row[4]
            if group == "":
                msg = str(nbr_rows)+" : has no US group."
                print(self.bcolors.WARNING+"row "+msg+self.bcolors.ENDC)
            if title == "":
                msg = "row " + str(nbr_rows)+" : has no title."
                print(self.bcolors.WARNING+msg+self.bcolors.ENDC)
            if load == "":
                msg = "row " + str(nbr_rows)+" : has no load."
                print(self.bcolors.WARNING+msg + self.bcolors.ENDC)

            id = self.get_char_id(self.ascci_indx_us)
            id = id+str(self.index_task)
            postit = self.Postit(group, tag, id, title, description, load)
            self.postits.append(postit)
            i = self.actual_US_index_in_postits-1
            self.postits[i].load = self.postits[i].load + int(load)
            self.nbr_postit = self.nbr_postit + 1
            self.index_task = self.index_task + 1

    def save_to_db(self):
        today = date.today().isoformat()
        self.when_i_create_back_log(project_name='CSV BackLog 1', done=False, last_edit_date=today)
        backlo_id = self.last_response.data['id']
        last_us_id = ""
        for postit in self.postits:
            if 'US' in postit.title:
                self.when_i_create_us(backlog_id=backlo_id, title=postit.title, id_us=postit.id, last_edit_date=today)
                last_us_id = postit.id
            else:
                us_id = US.objects.only('id').get(id_us=last_us_id).id
                self.when_i_create_task(backlog_id=backlo_id, us_id=us_id, tags=postit.tag, title=postit.title,
                                        id_task=postit.id, description=postit.description,
                                        load=postit.load, last_edit_date=today)

    # put each 6 postits in a single html page
    # if number of postits % 6 != 0 then create a rest in other html page
    def postits_array_to_html(self, html):
        # global cpt, document_nbr, data, postits, main_path
        for p in self.postits:
            # data.replace("$IDENTIFIER_"+str(cpt), p.group)
            # print("$IDENTIFIER_"+str(cpt))
            # TODO: handle case where cpt > 6
            # TODO: use similar lines just once
            if self.cpt == 1:
                html = self.replace_tags(p, html)
                self.cpt = self.cpt + 1
            elif (self.cpt > 1) and (self.cpt < 6):
                html = self.replace_tags(p, html)
                self.cpt = self.cpt + 1
            elif self.cpt == 6:
                html = self.replace_tags(p, html)
                html_path = self.main_path+'HTML_Output/test_'
                html_path = html_path+str(self.document_nbr)+'.html'
                f = open(html_path, 'w')
                f.write(html)
                f.close()
                html = self.data
                self.document_nbr = self.document_nbr + 1
                self.cpt = 1
        if self.cpt == 1:
            self.document_nbr = self.document_nbr - 1
        if self.cpt != 1:
            while self.cpt <= 6:
                html = html.replace(self._tag+str(self.cpt), ".")
                html = html.replace(self._id+str(self.cpt), ".")
                html = html.replace(self._title+str(self.cpt), ".")
                html = html.replace(self._desc+str(self.cpt), ".")
                html = html.replace(self._point+str(self.cpt), ".")
                self.cpt = self.cpt + 1
            html_path = self.main_path+'HTML_Output/test_'
            html_path = html_path+str(self.document_nbr)
            html_path = html_path+'.html'
            f = open(html_path, 'w')
            f.write(html)
            f.close()

    # convert each html page to pdf file
    # merge all pdf files in single one
    # after creating the result pdf file, remove pdf and html files
    def html_to_pdf(self):
        merger = PdfFileMerger()
        for x in range(self.document_nbr):
            pdf_path = (self.main_path+'PDF_Output/test'+str(x+1)+'.pdf')
            html_path = (self.main_path+'HTML_Output/test_'+str(x+1)+'.html')
            HTML(html_path).write_pdf(pdf_path)
            merger.append(pdf_path)
            os.remove(html_path)
            os.remove(pdf_path)

        date = datetime.datetime.now()
        str_date = "_" + date.strftime("%d-%m-%Y")
        result = "result_" + str_date + ".pdf"
        merger.write(self.main_path+"PDF_Output/" + result)
        merger.close()
        pdf_path = os.path.join(self.main_path, 'PDF_Output', result)
        msg = "Pdf has been generated successfully"
        print("🎉 " + self.bcolors.OKGREEN + msg + self.bcolors.ENDC)
        return pdf_path

    # save html code of the template html page as string
    def get_html_template(self):
        with open(self.main_path+'template/template.txt', 'r') as file:
            data = file.read()
        return data
