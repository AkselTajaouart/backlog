from datetime import date
from typing import Optional

from django.contrib.auth.models import User
from django.http import HttpResponse
from django.test import Client
from django.urls import reverse

from ..models import US, BackLog, Task


class PostitMixin:
    def any_back_log(self) -> BackLog:
        today = date.today()
        back_log = BackLog.objects.create(project_name="my first backlog", last_edit_date=today)
        self.any_us(back_log)
        self.any_us(back_log)
        return back_log

    def any_us(self, back_log: Optional[BackLog] = None) -> US:
        back_log = back_log or self.any_back_log()
        today = date.today()
        us = US(back_log=back_log, title="un titre", last_edit_date=today, id_us='A')
        self.any_task(us=us)
        self.any_task(us=us)
        us.full_clean()
        us.save()
        return us

    def any_task(self, us: Optional[US] = None) -> Task:
        us = us or self.any_us()
        today = date.today()
        id_task: str = "A2"
        tags: str = "tag1, tag2, tag3"
        title: str = "un tache"
        description: str = "une tache tres importante"
        load: int = 2
        task = Task(us=us, last_edit_date=today, id_task=id_task, tags=tags,
                    title=title, description=description, load=load)
        us.save()
        task.save()
        return task


class UserMixin:
    client: Client

    def any_superuser(self, logged_in: bool = True) -> User:
        user: User = User.objects.create_superuser('admin')
        if logged_in:
            self.client.force_login(user)
        return user


class AdminActionMixin:
    client: Client

    def when_i_complete(self, back_log: BackLog) -> HttpResponse:
        data = {'action': 'complete', '_selected_action': back_log}
        return self.client.post(reverse('admin:postits_backlog_changelist'), data, follow=True)
