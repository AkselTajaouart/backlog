from datetime import date

from django.test import TestCase
from hamcrest import has_property

from ..models import US, BackLog
from .fixtures import PostitMixin


class BackLogTests(TestCase, PostitMixin):
    def test_postit_str(self) -> None:
        back_log = self.any_bak_log()
        back_log.save()
        self.assertTrue(back_log.project_name in str(back_log))

    def test_back_log_has_2_entries(self) -> None:
        back_log = self.any_back_log()
        self.assertEquals(back_log.us_set.count(), 2)


class USTests(TestCase, PostitMixin):
    def test_postit_str(self) -> None:
        us = self.any_us()
        us.save()
        self.assertEquals(us.title, str(us))

    def test_us_is_saved(self) -> None:
        """
        any model instance returned from a fixture must be saved
        """
        today = date.today()
        back_log = BackLog.objects.create(project_name="my first backlog", last_edit_date=today)
        us = self.any_us(back_log=back_log)
        self.assertIsNotNone(us.id)


class TaskTests(TestCase, PostitMixin):
    def test_postit_str(self) -> None:
        task = self.any_task()
        task.save()
        self.assertEquals(task.title, str(task))

    def test_task_is_saved(self) -> None:
        """
        any model instance returned from a fixture must be saved
        """
        today = date.today()
        back_log = BackLog.objects.create(project_name="my first backlog", last_edit_date=today)
        us = US.objects.create(back_log=back_log, id_us="B", last_edit_date=today, title="user story")
        task = self.any_task(us=us)
        self.assertIsNotNone(task.id)
        self.assertIsNotNone(task.id)
        self.assertIsNotNone(task.id_task)
        self.assertIsNotNone(task.title)
        self.assertIsNotNone(task.tags)
        self.assertIsNotNone(task.description)
        self.assertIsNotNone(task.load)
        self.assert_that(us.task_set.all(), has_property('id_task', task.id_task))
