from django.test import TestCase
from hamcrest import assert_that, has_property, only_contains

from .fixtures import AdminActionMixin, PostitMixin, UserMixin


class CompleteTests(TestCase, PostitMixin, UserMixin, AdminActionMixin):
    def test_complete_back_log(self) -> None:
        """
        given
            - a not completed back log
            - a logged super user
        when
            - I perform the complete action

        then
            - http status is 200
            - the back log becomes completed
        """
        back_log = self.any_back_log()
        self.any_superuser(logged_in=True)

        response = self.when_i_complete(back_log)

        self.assertEqual(response.status_code, 200)
        assert_that(back_log, only_contains(has_property('done', True)))
