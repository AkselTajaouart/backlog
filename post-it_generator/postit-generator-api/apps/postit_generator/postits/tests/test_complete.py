from django.test import TestCase
from hamcrest import assert_that, has_property

from .fixtures import PostitMixin


class CompleteTests(TestCase, PostitMixin):
    def test_complete_back_log(self) -> None:
        """
        given a back_log
        when i complete this back_log
        then attribute done becomes true
        """
        back_log = self.any_back_log()
        back_log.complete()
        assert_that(back_log.done, has_property('done', True))

    def test_todo_list_contains_non_completed_tasks(self) -> None:
        """
        given a back log
        then it is not completed
        """
        back_log = self.any_back_log()
        assert_that(back_log, has_property('done', False))
