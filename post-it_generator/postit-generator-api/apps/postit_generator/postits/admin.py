from django.contrib import admin

from .models import US, BackLog, Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    model = Task


@admin.register(US)
class UserStoryAdmin(admin.ModelAdmin):
    model = US


class USinline(admin.TabularInline):
    model = US
    extra = 0


@admin.register(BackLog)
class BacklogAdmin(admin.ModelAdmin):
    inlines = (USinline, )

    def complete(self, request, queryset):
        for back_log in queryset:
            back_log.complete()
    complete.short_description = "Set BackLog to completed"  # type: ignore

    actions = [complete]
