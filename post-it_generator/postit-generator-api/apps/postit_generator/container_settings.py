# flake8: noqa
import os

from postit_generator.settings import *

DEBUG = True

INSTALLED_APPS += (
)


MEDIA_URL = "{}/media/".format(os.environ.get('PUBLIC_ENDPOINT', 'http://localhost:8000'))

ALLOWED_HOSTS += ["postit-generator-api"]
