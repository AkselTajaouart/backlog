# flake8: noqa
from postit_generator.settings import *

DEBUG = True

TEST_RUNNER = 'xmlrunner.extra.djangotestrunner.XMLTestRunner'
TEST_OUTPUT_DIR = os.getenv('TEST_OUTPUT_DIR', '/tmp')
TEST_OUTPUT_FILE_NAME = os.getenv('TEST_OUTPUT_FILE_NAME', 'tests_results.xml')

# Use a fast hasher to speed up tests.
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]
