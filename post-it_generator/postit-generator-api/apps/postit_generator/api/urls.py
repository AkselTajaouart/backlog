from django.urls import include, path

app_name = 'api'

urlpatterns = [
    path('v10/', include('postit_generator.api.v10.urls')),
]
