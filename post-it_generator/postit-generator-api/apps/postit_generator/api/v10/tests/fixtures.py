from django.test import Client
from django.urls import reverse
from rest_framework.response import Response


class PostitAPIMixin:
    last_response: Response
    client: Client

    def when_i_list_back_logs(self) -> 'PostitAPIMixin':
        self.last_response = self.client.get(reverse('api:v10:backlog-list'))
        return self

    def when_i_get_backlog(self, id: int) -> 'PostitAPIMixin':
        self.last_response = self.client.get(reverse('api:v10:backlog-detail', args=[id]))
        return self

    def when_i_create_back_log(self, project_name: str, done: bool, last_edit_date: str) -> 'PostitAPIMixin':
        self.last_response = self.client.post(reverse('api:v10:backlog-list'), data={'project_name': project_name,
                                                                                     'done': done,
                                                                                     'last_edit_date': last_edit_date})
        return self

    def when_i_create_us(self, backlog_id: int, title: str, id_us: str, last_edit_date: str) -> 'PostitAPIMixin':
        self.last_response = self.client.post(reverse('api:v10:uss-list', args=(backlog_id,)),
                                              data={'title': title,
                                                    'id_us': id_us,
                                                    'last_edit_date': last_edit_date})
        return self

    def when_i_create_task(self, backlog_id: int, us_id: str, tags: str, title: str, id_task: str,
                           description: str, load: int, last_edit_date: str) -> 'PostitAPIMixin':
        self.last_response = self.client.post(reverse('api:v10:tasks-list', kwargs={'us_id': us_id,
                                                                                    'backlog_id': backlog_id}),
                                              data={'id_task': id_task,
                                                    'tags': tags,
                                                    'title': title,
                                                    'description': description,
                                                    'load': load,
                                                    'us_id': us_id,
                                                    'last_edit_date': last_edit_date})
        return self

    def when_i_update_task(self, backlog_id: int, us_id: int, task_id: int,  tags: str, title: str, id_task: str,
                           description: str, load: int, last_edit_date: str) -> 'PostitAPIMixin':
        self.last_response = self.client.put(reverse('api:v10:tasks-detail', kwargs={'us_id': us_id,
                                                                                     'backlog_id': backlog_id,
                                                                                     'pk': task_id}),
                                             content_type='application/json',
                                             data={'id_task': id_task,
                                                   'tags': tags,
                                                   'title': title,
                                                   'description': description,
                                                   'load': load,
                                                   'us_id': us_id,
                                                   'last_edit_date': last_edit_date})
        return self

    def when_i_delete_task(self, backlog_id: int, us_id: int, task_id: int) -> 'PostitAPIMixin':
        self.last_response = self.client.delete(reverse('api:v10:tasks-detail', kwargs={'us_id': us_id,
                                                                                        'backlog_id': backlog_id,
                                                                                        'pk': task_id}))
        return self

    def when_i_delete_us(self, backlog_id: int, us_id: int) -> 'PostitAPIMixin':
        self.last_response = self.client.delete(reverse('api:v10:uss-detail', kwargs={'backlog_id': backlog_id,
                                                                                      'pk': us_id}))
        return self

    def when_i_delete_backlog(self, backlog_id: int) -> 'PostitAPIMixin':
        self.last_response = self.client.delete(reverse('api:v10:backlog-detail', kwargs={'pk': backlog_id}))
        return self

    def when_i_post_CSV(self, project_name: str, txt_backlog: str) -> 'PostitAPIMixin':
        self.last_response = self.client.post(reverse('api:v10:csv-list'), content_type='application/json',
                                              data={'project_name': project_name, 'txt_backlog': txt_backlog})
        return self
