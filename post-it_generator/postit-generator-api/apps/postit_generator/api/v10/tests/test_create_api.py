from datetime import date

from django.test import TestCase
from hamcrest import (
    anything, assert_that, has_entries, has_entry, has_item, has_length, is_,
)
from rest_framework import status

from postit_generator.postits.tests.fixtures import PostitMixin

from .fixtures import PostitAPIMixin


class CreateBackLogAPITests(TestCase, PostitMixin, PostitAPIMixin):
    def test_create(self) -> None:
        """
        when I create a back_log named 'mobile application'
        then I get an http 201
            containing a list named `mobile application`
        """

        today = date.today().isoformat()
        self.when_i_create_back_log(project_name='mobile application 1', done=False, last_edit_date=today)
        assert_that(self.last_response.status_code, is_(status.HTTP_201_CREATED))
        assert_that(self.last_response.data, has_entry('project_name', 'mobile application 1'))

    def test_create_existing_project_name(self) -> None:
        """
        given any backlog
        when I create a backlog with the same name
        then I get an http 400
        containing an error on the project_name field
        """
        backlog = self.any_back_log()
        today = date.today().isoformat()
        self.when_i_create_back_log(backlog.project_name, done=False, last_edit_date=today)
        assert_that(self.last_response.status_code, is_(status.HTTP_400_BAD_REQUEST))
        assert_that(self.last_response.data, has_entry('project_name', anything()))

    def test_create_empty(self) -> None:
        """
        when I create a list named ''
        then I get an http 400
        containing an error on the name field
        """
        today = date.today().isoformat()
        self.when_i_create_back_log(project_name='', done=False, last_edit_date=today)
        assert_that(self.last_response.status_code, is_(status.HTTP_400_BAD_REQUEST))


class CreateUSerStoryAPITests(TestCase, PostitAPIMixin, PostitMixin):
    def test_create_us(self) -> None:
        """
        given any backlog
        when I create a new us with title 'toto'
        then :
            - I get an http 201
            - Response contains a backlog, with a  us titled as toto
        """
        backlog = self.any_back_log()
        today = date.today().isoformat()
        self.when_i_create_us(backlog_id=backlog.id, title='toto', last_edit_date=today, id_us="ZZ5")
        assert_that(self.last_response.status_code, is_(status.HTTP_201_CREATED))
        assert_that(self.last_response.data['uss'], has_item(has_entries({'title': 'toto',
                                                                          'id_us': 'ZZ5'})))

    def test_create_empty_us(self) -> None:
        """
        given any backlog
        when I create a new us with title ''
        then :
            - I get an http 400
        """
        backlog = self.any_back_log()
        today = date.today().isoformat()
        self.when_i_create_us(backlog_id=backlog.id, title='', id_us="X9", last_edit_date=today)
        assert_that(self.last_response.status_code, is_(status.HTTP_400_BAD_REQUEST))

    def test_create_task_twice(self) -> None:
        """
        given any list
        when I create a new todo with title 'toto' twice
        then :
            - I get an http 201
            - Response contains a backlog, with 2 us titled as toto
        """
        backlog = self.any_back_log()
        today = date.today().isoformat()
        self.when_i_create_us(backlog_id=backlog.id, title='toto', id_us="A19", last_edit_date=today)
        self.when_i_create_us(backlog_id=backlog.id, title='toto', id_us="A99", last_edit_date=today)
        assert_that(self.last_response.status_code, is_(status.HTTP_201_CREATED))
        user_stories_matcher = [us for us in self.last_response.data['uss'] if us['title'] == 'toto']
        assert_that(user_stories_matcher, has_length(2))


class CreateTaskAPITests(TestCase, PostitAPIMixin, PostitMixin):
    def test_create_task(self) -> None:
        """
        given any us
        when I create a new task
        then :
            - I get an http 201
            - Response contains a us, with a task with same attributes
        """

        backlog = self.any_back_log()
        us = self.any_us(back_log=backlog)
        tags = "tag1, tag2"
        title = "title of task"
        id_task = "A11"
        description = "task desc"
        load = 2
        today = date.today().isoformat()

        self.when_i_create_task(backlog_id=backlog.id, us_id=us.id, tags=tags, title=title,
                                id_task=id_task, description=description,
                                load=load, last_edit_date=today)
        assert_that(self.last_response.status_code, is_(status.HTTP_201_CREATED))
        assert_that(self.last_response.data['tasks'][0], has_entries({'load': 2,
                                                                     'tags': 'tag1, tag2',
                                                                      'title': 'title of task',
                                                                      'id_task': 'A11',
                                                                      'description': 'task desc'
                                                                      }))

    def test_create_empty_task(self) -> None:
        """
        given any list
        when I create a new task with title ''
        then :
            - I get an http 201
            - Response contains a us, with a task that has an empty title
        """
        backlog = self.any_back_log()
        us = self.any_us(back_log=backlog)
        tags = "tag1, tag2a"
        title = "title of taska"
        id_task = ""
        description = "task desca"
        load = 2
        today = date.today().isoformat()

        self.when_i_create_task(backlog_id=backlog.id, us_id=us.id, tags=tags, title=title,
                                id_task=id_task, description=description,
                                load=load, last_edit_date=today)
        assert_that(self.last_response.status_code, is_(status.HTTP_400_BAD_REQUEST))
        # assert_that(self.last_response.data, has_entry('model', 'us'))
        # assert_that(self.last_response.data['tasks'], has_item(has_entries({'title': ''})))

    def test_create_task_twice(self) -> None:
        """
        given any list
        when I create a new todo with title 'toto' twice
        then :
            - I get an http 201
            - Response contains a us, with 2  tasks described as toto
        """
        backlog = self.any_back_log()
        us = self.any_us(back_log=backlog)
        today = date.today().isoformat()

        self.when_i_create_task(backlog_id=backlog.id, us_id=us.id, tags='tag 1', title='a title',
                                id_task='task_id 1', description='desc 1',
                                load=1, last_edit_date=today)
        self.when_i_create_task(backlog_id=backlog.id, us_id=us.id, tags='tag 2', title='a title',
                                id_task='task_id 2', description='desc 2',
                                load=2, last_edit_date=today)
        assert_that(self.last_response.status_code, is_(status.HTTP_201_CREATED))
        # assert_that(self.last_response.data, has_entry('model', 'us'))
        tasks_matcher = [task for task in self.last_response.data['tasks'] if task['title'] == 'a title']
        assert_that(tasks_matcher, has_length(2))


class PostCsvAPITests(TestCase, PostitAPIMixin, PostitMixin):
    def test_cpost_csv(self) -> None:
        txt_backlog = "aaaaaa;bbbbb;ccccc;dddddd;00000000\n11111111;22222222;3333333;444444;5555555"""
        self.when_i_post_CSV(project_name='project name from csv',
                             txt_backlog=txt_backlog)


#   fix : def test_create_task_when_todolist_does_not_exist(self) -> None:
