from django.test import TestCase
from hamcrest import assert_that, contains_inanyorder, has_entries

from postit_generator.postits.tests.fixtures import PostitMixin

from ..serializers import BackLogSerializer


class BackLogSerializerTests(TestCase, PostitMixin):
    serializer_class = BackLogSerializer

    def test_serialize(self) -> None:
        back_log = self.any_back_log()
        data = self.serializer_class(back_log).data
        assert_that(data, has_entries('project_name', back_log.project_name))

        assert_that(data['uss'], contains_inanyorder(*[has_entries(

                                                                                {
                                                                                    'id': us.id,
                                                                                    'title': us.title,
                                                                                    'id_us': us.id_us
                                                                                }
                                                                               )for us in back_log.us_set.all()]))

        # todo : assert 'fields']['uss']['tasks']
