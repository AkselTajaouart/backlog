from django.test import TestCase
from hamcrest import assert_that, has_entries, has_item, is_
from rest_framework import status

from postit_generator.postits.tests.fixtures import PostitMixin

from .fixtures import PostitAPIMixin


class UpdateTodoAPITests(TestCase, PostitAPIMixin, PostitMixin):
    """
    given any task
    when I update task's title to "tag 99"
    then :
        - I get an http 201
        - Response contains a US, with task titled "tag 99"
    """
    def test_update_task(self) -> None:
        task = self.any_task()
        self.when_i_update_task(backlog_id=task.us.back_log.id, us_id=task.us.id, task_id=task.id, title='tag 99',
                                tags=task.tags, id_task=task.id_task, description=task.description,
                                load=task.load, last_edit_date=task.last_edit_date)
        assert_that(self.last_response.status_code, is_(status.HTTP_201_CREATED))
        assert_that(self.last_response.data['tasks'], has_item(has_entries({'title': 'tag 99'})))

    """
    when I update a non existing tasks title
    then :
        - I get a http 404
    """
    def test_update_task_that_does_not_exist(self) -> None:
        task = self.any_task()
        self.when_i_update_task(backlog_id=task.us.back_log.id, us_id=task.us.id, task_id=99999, title='tag 99',
                                tags=task.tags, id_task=task.id_task, description=task.description,
                                load=task.load, last_edit_date=task.last_edit_date)
        assert_that(self.last_response.status_code, is_(status.HTTP_404_NOT_FOUND))

    """
    given any task
    when I update a task to the same value
    then :
        - I get an http 201
        - Response contains a us, with  task with same title
    """
    def test_update_task_to_same_value(self) -> None:
        task = self.any_task()
        self.when_i_update_task(backlog_id=task.us.back_log.id, us_id=task.us.id, task_id=task.id, title=task.title,
                                tags=task.tags, id_task=task.id_task, description=task.description,
                                load=task.load, last_edit_date=task.last_edit_date)
        assert_that(self.last_response.status_code, is_(status.HTTP_201_CREATED))
        assert_that(self.last_response.data['tasks'], has_item(has_entries({'title': task.title})))

    """
    given any task
    when I update task on wrong us
    then :
        - I get a 404
    """
    def test_update_task_with_wrong_us(self) -> None:
        task = self.any_task()
        self.when_i_update_task(backlog_id=99999, us_id=99999, task_id=task.id, title='tag 99',
                                tags=task.tags, id_task=task.id_task, description=task.description,
                                load=task.load, last_edit_date=task.last_edit_date)
        assert_that(self.last_response.status_code, is_(status.HTTP_404_NOT_FOUND))
