from django.test import TestCase
from django.urls import reverse
from hamcrest import assert_that, is_


class PingAPITests(TestCase):

    def test_ping_api(self) -> None:
        """
        when i get on ping api
        then i get http 200
        """
        resp = self.client.get(reverse('api:v10:ping'))
        assert_that(resp.status_code, is_(200))
