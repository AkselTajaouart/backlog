from django.test import TestCase
from hamcrest import assert_that, is_

from postit_generator.postits.tests.fixtures import PostitMixin

from .fixtures import PostitAPIMixin


class APITests(TestCase, PostitAPIMixin, PostitMixin):

    def test_list_backlogs(self) -> None:
        """
        given any backlog
        when I list backlogs
        then
            I get an HTTP 200
            ... containing a single back log
        """
        self.any_back_log()
        self.when_i_list_back_logs()
        assert_that(self.last_response.status_code, is_(200))

    def test_get_backlog_by_id(self) -> None:
        """
        given any back log
        when I get this backlog by id
        then
            I get an HTTP 200
            ... containing a single back log
            ... whose pk is the backlog id
        """
        back_log = self.any_back_log()
        self.when_i_get_backlog(back_log.id)
        assert_that(self.last_response.status_code, is_(200))

    def test_get_backlog_by_id__dont_exist(self) -> None:
        """
        when I get backlog with id 123
        then I get an HTTP 404
        """
        self.when_i_get_backlog(id=123)
        assert_that(self.last_response.status_code, is_(404))
