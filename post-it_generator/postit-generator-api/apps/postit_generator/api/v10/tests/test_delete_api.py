from django.test import TestCase
from hamcrest import assert_that, empty, has_length, is_
from rest_framework import status

from postit_generator.postits.models import US
from postit_generator.postits.tests.fixtures import PostitMixin

from .fixtures import PostitAPIMixin


class DeleteTaskAPITests(TestCase, PostitMixin, PostitAPIMixin):
    """
    given a task
    when i delete this task
    then :
        - i get a http 200
        - us task_set is -1
    """
    def test_delete_task(self):
        task = self.any_task()
        us_length = len(task.us.task_set.all())
        self.when_i_delete_task(backlog_id=task.us.back_log.id, us_id=task.us.id, task_id=task.id)
        assert_that(self.last_response.status_code, is_(status.HTTP_200_OK))
        assert_that(self.last_response.data['tasks'], has_length(us_length-1))

    """
    when i delete a non existing task
    then i get a http 404
    """
    def test_delete_task_that_does_not_exist(self) -> None:
        task = self.any_task()
        self.when_i_delete_task(backlog_id=task.us.back_log.id, us_id=task.us.id, task_id=99999)
        assert_that(self.last_response.status_code, is_(status.HTTP_404_NOT_FOUND))

    """
    given a task
    when I delete this task on the wrong us
    then :
        - I get a 404
    """
    def test_delete_todo_with_wrong_list(self) -> None:
        task = self.any_task()
        self.when_i_delete_task(backlog_id=task.us.back_log.id, us_id=99999, task_id=task.id)
        assert_that(self.last_response.status_code, is_(status.HTTP_404_NOT_FOUND))

    """
    given a us
    when I delete all tasks
    then the task list in us is empty
    """
    def test_delete_task_that_empties_us(self) -> None:
        us = self.any_us()
        self.any_task(us=us)
        [self.when_i_delete_task(backlog_id=us.back_log.id,
                                 us_id=us.id,
                                 task_id=task.id)
         for task in us.task_set.all()]
        us.save()
        assert_that(self.last_response.data['tasks'], has_length(0))


class DeleteUserStoryAPITests(TestCase, PostitMixin, PostitAPIMixin):
    """
    given a us
    when i delete this us
    then :
        - i get a http 200
        - backlock us_set is -1
    """
    def test_delete_us(self):
        us = self.any_us()
        backlog_length = len(us.back_log.us_set.all())
        self.when_i_delete_us(backlog_id=us.back_log.id, us_id=us.id)
        assert_that(self.last_response.status_code, is_(status.HTTP_200_OK))
        assert_that(self.last_response.data['uss'], has_length(backlog_length-1))

    """
    when i delete a non existing us
    then i get a http 404
    """
    def test_delete_us_that_does_not_exist(self) -> None:
        us = self.any_us()
        self.when_i_delete_us(backlog_id=us.back_log.id, us_id=99999)
        assert_that(self.last_response.status_code, is_(status.HTTP_404_NOT_FOUND))

    """
    given a us
    when I delete this us on the wrong backlog
    then :
        - I get a 404
    """
    def test_delete_us_with_wrong_backlog(self) -> None:
        us = self.any_us()
        self.when_i_delete_us(backlog_id=us.back_log.id, us_id=99999)
        assert_that(self.last_response.status_code, is_(status.HTTP_404_NOT_FOUND))

    """
    given a backlog
    when I delete all user stories
    then the the backlog is empty
    """
    def test_delete_us_that_empties_backlog(self) -> None:
        backlog = self.any_back_log()
        self.any_us(back_log=backlog)
        [self.when_i_delete_us(backlog_id=backlog.id, us_id=us.id)
         for us in backlog.us_set.all()]
        backlog.save()
        assert_that(self.last_response.data['uss'], has_length(0))


class DeleteBackLogApiTests(TestCase, PostitMixin, PostitAPIMixin):
    """
    given a backlog
    when i delete this backlog
    then
        - I get a http 200 response
        - every nested us have been deleted
    """
    def test_delete_backlog(self):
        backlog = self.any_back_log()
        user_storiers = backlog.us_set.all()
        self.when_i_delete_backlog(backlog_id=backlog.id)
        assert_that(self.last_response.status_code, is_(status.HTTP_200_OK))
        deleted_todos = [US.objects.get(us) for us in user_storiers]
        assert_that(deleted_todos, empty())

    """
    when i delete a not existing backlog
    then i get a http 404 response
    """
    def test_delete_backlog_does_not_exist(self):
        self.when_i_delete_backlog(backlog_id=99999)
        assert_that(self.last_response.status_code, is_(status.HTTP_404_NOT_FOUND))
