from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from postit_generator.postits.models import CSV
from postit_generator.postits.tests.fixtures import US, BackLog, Task


class TaskSerializer(serializers.ModelSerializer):
    # label = serializers.CharField(source='title')

    class Meta:
        model = Task
        model_name = 'task'
        fields = ('id', 'id_task', 'tags', 'title', 'last_edit_date', 'description', 'load')


class USSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True, source='task_set')

    class Meta:
        model = US
        model_name = 'us'
        fields = ('id', 'tasks', 'id_us', 'title', 'last_edit_date', )


class BackLogSerializer(serializers.ModelSerializer):
    uss = USSerializer(many=True, source='us_set')

    class Meta:
        model = BackLog
        model_name = 'backlog'
        fields = ('id', 'project_name', 'last_edit_date', 'done', 'uss')


class CsvSerializer(serializers.ModelSerializer):

    class Meta:
        model = CSV
        model_name = 'CSV'
        fields = ('project_name', 'txt_backlog')


class CreateCSVSerializer(serializers.ModelSerializer):
    project_name = serializers.CharField()
    txt_backlog = serializers.CharField()

    class Meta:
        model = CSV
        fields = ('project_name', 'txt_backlog', )


class CreateBackLogSerializer(serializers.ModelSerializer):
    project_name = serializers.CharField()

    def validate_project_name(self, value):
        if BackLog.objects.filter(project_name=value).exists():
            raise ValidationError('already exists')
        return value

    class Meta:
        model = BackLog
        fields = ('project_name', 'last_edit_date', )


class CreateUserStorySerializer(serializers.Serializer):
    title = serializers.CharField()
    id_us = serializers.CharField()
    last_edit_date = serializers.DateField()

    # def validate_id_us(self, value):
    def create(self, back_log: BackLog) -> BackLog:
        user_story = US(back_log=back_log, **self.validated_data)
        user_story.full_clean()
        user_story.save()
        return back_log


class CreateTaskSerializer(serializers.Serializer):
    last_edit_date = serializers.DateField()
    title = serializers.CharField()
    tags = serializers.CharField()
    description = serializers.CharField()
    load = serializers.IntegerField()
    id_task = serializers.CharField()

    def create(self, us: US, back_log: BackLog) -> US:
        task = Task(us=us, **self.validated_data)
        task.full_clean()
        task.save()
        return us


class UpdateTaskSerializer(serializers.Serializer):
    last_edit_date = serializers.DateField()
    title = serializers.CharField()
    tags = serializers.CharField()
    description = serializers.CharField()
    load = serializers.IntegerField()
    id_task = serializers.CharField()

    def save(self, instance: Task) -> Task:
        instance.last_edit_date = self.validated_data['last_edit_date']
        instance.title = self.validated_data['title']
        instance.tags = self.validated_data['tags']
        instance.description = self.validated_data['description']
        instance.load = self.validated_data['load']
        instance.id_task = self.validated_data['id_task']
        instance.full_clean()
        instance.save()
        return instance
