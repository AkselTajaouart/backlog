from django.shortcuts import get_object_or_404
from rest_framework import mixins, status, viewsets
from rest_framework.response import Response

from postit_generator.api.v10.tests.fixtures import PostitAPIMixin
from postit_generator.postits.models import US, BackLog, PostItGenerator, Task

from .serializers import (
    BackLogSerializer, CreateBackLogSerializer, CreateTaskSerializer,
    CreateUserStorySerializer, UpdateTaskSerializer, USSerializer,
    CsvSerializer,
)


class CsvViewSet(mixins.ListModelMixin,
                 mixins.RetrieveModelMixin,
                 viewsets.GenericViewSet,
                 PostitAPIMixin):
    """
    A simple ViewSet for viewing backlogs.
    """
    serializer_class = CsvSerializer
    create_serializer_class = CreateBackLogSerializer

    def create(self, request, *args, **kargs):
        csv = request.data['txt_backlog']
        # file_name = request.data['project_name']

        postit_generator = PostItGenerator()
        postit_generator.setup_folders()
        postit_generator.read_csv(csv)
        postit_generator.save_to_db()
        # postit_generator.data = postit_generator.get_html_template()
        # html = postit_generator.data
        # postit_generator.postits_array_to_html(html)
        # return postit_generator.html_to_pdf()
        return Response(status=status.HTTP_201_CREATED)


class BackLogViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    """
    A simple ViewSet for viewing backlogs.
    """
    queryset = BackLog.objects.all()
    serializer_class = BackLogSerializer
    create_serializer_class = CreateBackLogSerializer

    def create(self, request, *args, **kargs):
        serializer = self.create_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance: BackLog = serializer.save()
        return Response(self.serializer_class(instance).data, status=status.HTTP_201_CREATED)

    def delete(self, request, pk=None):
        assert pk is not None
        backlog = get_object_or_404(BackLog, pk=pk)
        backlog.delete()
        return Response(self.serializer_class(backlog).data, status=status.HTTP_200_OK)


class UserStoryViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    A simple ViewSet for viewing US
    """
    create_serializer_class = CreateUserStorySerializer
    serializer_class = BackLogSerializer

    def create(self, request, backlog_id):
        serializer = self.create_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        backlog_list = BackLog.objects.filter(id=backlog_id).first()
        instance: BackLog = serializer.create(backlog_list)
        return Response(self.serializer_class(instance).data, status=status.HTTP_201_CREATED)

    def update(self, request, backlog_id=None, pk=None):
        assert backlog_id is not None
        assert pk is not None
        us = get_object_or_404(US, pk=pk, backlog_id=backlog_id)
        serializer = self.update_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        us = serializer.save(us)
        return Response(self.serializer_class(us.back_log).data, status=status.HTTP_201_CREATED)

    def delete(self, request, backlog_id=None, pk=None):
        assert backlog_id is not None
        assert pk is not None
        us = get_object_or_404(US, pk=pk, back_log_id=backlog_id)
        us.delete()
        return Response(self.serializer_class(us.back_log).data, status=status.HTTP_200_OK)


class TaskViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    A simple ViewSet for viewing Task
    """
    create_serializer_class = CreateTaskSerializer
    serializer_class = USSerializer
    update_serializer_class = UpdateTaskSerializer

    def create(self, request, backlog_id=None, us_id=None):
        # assert backlog_id is not None
        # back_log = get_object_or_404(BackLog, pk=backlog_id)
        # assert us_id is not None
        # us = get_object_or_404(BackLog, pk=backlog_id)
        serializer = self.create_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        back_log: BackLog = BackLog.objects.filter(id=backlog_id).first()
        us = get_object_or_404(US, id=us_id)
        # ipdb.set_trace
        instance: BackLog = serializer.create(us=us, back_log=back_log)
        return Response(self.serializer_class(instance).data, status=201)

    def update(self, request, backlog_id=None, us_id=None, pk=None):
        assert us_id is not None
        assert pk is not None
        task = get_object_or_404(Task, pk=pk, us_id=us_id)
        serializer = self.update_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        task = serializer.save(task)
        return Response(self.serializer_class(task.us).data, status=status.HTTP_201_CREATED)

    def delete(self, request, backlog_id=None, us_id=None, pk=None):
        assert us_id is not None
        assert pk is not None
        task = get_object_or_404(Task, pk=pk, us_id=us_id)
        task.delete()
        return Response(self.serializer_class(task.us).data, status=status.HTTP_200_OK)
