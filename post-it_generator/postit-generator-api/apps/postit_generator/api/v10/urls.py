from django.http import HttpResponse
from django.urls import path
from django.views.generic import View
from rest_framework import routers

from .views import BackLogViewSet, CsvViewSet, TaskViewSet, UserStoryViewSet

app_name = 'v10'

router = routers.DefaultRouter()
router.register(r'backlogs', BackLogViewSet)
router.register(r'csv', CsvViewSet, basename='csv')
router.register(r'backlogs/(?P<backlog_id>\w+)/uss', UserStoryViewSet, basename='uss')
router.register(r'backlogs/(?P<backlog_id>\w+)/uss/(?P<us_id>\w+)/tasks', TaskViewSet, basename='tasks')


class PingView(View):
    def get(self, request):
        return HttpResponse('wokrs')


urlpatterns = [
    path('ping/', PingView.as_view(), name='ping'),
]

urlpatterns += router.urls
