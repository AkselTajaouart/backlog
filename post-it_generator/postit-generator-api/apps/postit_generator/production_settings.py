# flake8: noqa
from postit_generator.settings import *

DEBUG = False
TEMPLATE_DEBUG = False

STATIC_ROOT = '/usr/local/ripple/postit-generator.ripplemotion.fr/shared/public/static/'
MEDIA_ROOT = '/usr/local/ripple/postit-generator.ripplemotion.fr/shared/public/media/'

MEDIA_URL = 'https://postit-generator.ripplemotion.fr/media/'

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

# FIXME Change according to project
RAVEN_CONFIG['dsn'] = ''

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
