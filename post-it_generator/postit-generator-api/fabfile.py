# coding: utf-8

from __future__ import unicode_literals

from deploy import *  # noqa ignore=F405
from fabric.api import env, task


env.env_file = ''
env.procfile = 'Procfile'
env.db_url = 'postgres://ripple@localhost:5432/postit_generator'
env.broker_url = 'amqp://ripple:0ripple1@localhost:5672/postit_generator'


@task(aliases=['dev', 'inte'])
def integration():
    """
    configure hosts to target dev environment
    """
    env.user = 'ripple'
    env.app_name = 'postit-generator-dev.ripplemotion.fr'
    env.hosts = ['', ]
    env.api_master = ''
    env.env_file = 'integration.env'
    env.root = '/usr/local/ripple/{}/'.format(env.app_name)
    env.nginx_file = 'conf/nginx/{}.conf'.format(env.app_name)
    env.logrotate_file = 'conf/logrotate/{}.conf'.format(env.app_name)


@task(aliases=['prod'])
def production():
    """
    configure hosts to target prod environment
    """
    env.user = 'ripple'
    env.app_name = 'postit-generator.ripplemotion.fr'
    env.hosts = ['', ]
    env.api_master = ''
    env.env_file = 'production.env'
    env.root = '/usr/local/ripple/{}/'.format(env.app_name)
    env.nginx_file = 'conf/nginx/{}.conf'.format(env.app_name)
    env.logrotate_file = 'conf/logrotate/{}.conf'.format(env.app_name)
