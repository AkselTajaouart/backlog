//
//  BackLogTableViewController.swift
//  postits
//
//  Created by lpirm02 on 01/04/2020.
//  Copyright © 2020 IUT. All rights reserved.
//

import UIKit
import MobileCoreServices


class BackLogTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIDocumentPickerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    
    var list_backlogs: backlogs? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        loadBackLogs()
    }
    
    

    @IBAction func refresh(_ sender: Any) {
        loadBackLogs()
    }
    

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list_backlogs?.count ?? 0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "BackLogTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? BackLogTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        let backlog = list_backlogs?[indexPath.row]
        
        cell.projectName.text = backlog?.projectName
        cell.number_postits.text = String(backlog?.uss.count ?? 0) + " US"
        cell.last_edit_date.text = backlog?.lastEditDate

        return cell
    }
    

    func loadBackLogs(){
       if let url = URL(string: "http://localhost:8000/api/v10/backlogs/?format=json") {
           URLSession.shared.dataTask(with: url) { data, response, error in
              if let data = data {
                self.list_backlogs = try? JSONDecoder().decode(backlogs.self, from: data)
                DispatchQueue.main.async {
                   self.tableView.reloadData()
                }
               }
           }.resume()
        }
    }
    
    // MARK: - WelcomeElement
    struct BackLog: Codable {
        let id: Int
        let projectName, lastEditDate: String
        let done: Bool
        let uss: [US]

        enum CodingKeys: String, CodingKey {
            case id
            case projectName = "project_name"
            case lastEditDate = "last_edit_date"
            case done, uss
        }
    }

    // MARK: - Uss
    struct US: Codable {
        let id: Int
        let tasks: [Task]
        let idUs, title, lastEditDate: String

        enum CodingKeys: String, CodingKey {
            case id, tasks
            case idUs = "id_us"
            case title
            case lastEditDate = "last_edit_date"
        }
    }

    // MARK: - Task
    struct Task: Codable {
        let id: Int
        let idTask, tags, title, lastEditDate: String
        let taskDescription: String
        let load: Int

        enum CodingKeys: String, CodingKey {
            case id
            case idTask = "id_task"
            case tags, title
            case lastEditDate = "last_edit_date"
            case taskDescription = "description"
            case load
        }
    }
    
    typealias backlogs = [BackLog]

}

