//
//  PostViewController.swift
//  postits
//
//  Created by lpirm02 on 01/04/2020.
//  Copyright © 2020 IUT. All rights reserved.
//

import UIKit


class PostViewController: UIViewController {

    let txt_backlog = "aaaaaa;bbbbb;ccccc;dddddd;00000000\n11111111;22222222;3333333;444444;5555555"
    let project_name = "project name from csv"
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        post()
        //let _ = "http://localhost:8000/api/v10/csv/"
    }
    
    
    
    func post(){
        
        struct csv: Codable {
            let txt_backlog: String
            let project_name: String
        }

        // ...

        let backlog = csv(txt_backlog: self.txt_backlog,
                        project_name: self.project_name)
        guard let uploadData = try? JSONEncoder().encode(backlog) else {
            return
        }
        
        let url = URL(string: "http://localhost:8000/api/v10/csv/")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        /request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        
        
        
        let task = URLSession.shared.uploadTask(with: request, from: uploadData) { data, response, error in
            if let error = error {
                print ("error: \(error)")
                return
            }
            guard let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode) else {
                print ("------------- server error")
                return
            }
    
        }
        task.resume()
        
        
        
        
    }
    

    
}
