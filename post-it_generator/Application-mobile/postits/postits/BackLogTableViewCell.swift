//
//  BackLogTableViewCell.swift
//  postits
//
//  Created by lpirm02 on 01/04/2020.
//  Copyright © 2020 IUT. All rights reserved.
//

import UIKit

class BackLogTableViewCell: UITableViewCell {

    @IBOutlet weak var projectName: UILabel!
    
    @IBOutlet weak var last_edit_date: UILabel!
    @IBOutlet weak var number_postits: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
